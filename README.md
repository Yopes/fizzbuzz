# FizzBuzz
To execute the FizzBuzz server run:
```
go run main.go
```

It will run on the port `8080`

## /fizzbuzz
This route takes 5 query parameters: 
- `str1`: string
- `str2`: string
- `int1`: integer > 0
- `int2`: integer > 0
- `limit`: integer > 0

This route returns the result of a FizzBuzz in the following format:
```json
["1","fizz","buzz","fizz","5","fizzbuzz"]
```

## /statistics
This route returns an array of every configuration ran and their number of hits, in the following format:
```json
[{"int1":2,"int2":3,"str1":"fizz","str2":"buzz","limit":6,"number_of_hits":4},{"int1":2,"int2":3,"str1":"foo","str2":"bar","limit":6,"number_of_hits":1}]
```

