package server

import (
	"fizzbuzz/models"
	"fizzbuzz/server/handlers"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Context is the context passed to the handler
type Context struct {
	context *gin.Context
}

// QueryParam returns a string value of a query parameter
func (ctx *Context) QueryParam(name string) string {
	return ctx.context.Query(name)
}

// Server is an abstraction of a http server
type Server struct {
	engine *gin.Engine
	models models.IModels
}

// New creates and initializes a new Server
func New(models models.IModels) Server {
	engine := gin.Default()
	return Server{engine: engine, models: models}
}

// Subscribe registers a method and a route to a handler
func (s *Server) Subscribe(method string, route string, handler handlers.Handler) {
	switch method {
	case http.MethodGet:
		s.engine.GET(route, func(ctx *gin.Context) {
			statusCode, res := handler(&Context{context: ctx}, s.models)
			ctx.JSON(statusCode, res)
		})
	}
}

// Run starts the http Server
func (s *Server) Run(port int) error {
	return s.engine.Run(fmt.Sprintf(":%d", port))
}
