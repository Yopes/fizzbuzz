package handlers_test

import (
	"encoding/json"
	"errors"
	"fizzbuzz/mocks/mock_models"
	"fizzbuzz/models"
	"fizzbuzz/server/handlers"
	"net/http"
	"testing"

	"github.com/golang/mock/gomock"
)

const expectedStatisticsResult = `[{"int1":1,"int2":2,"str1":"str1","str2":"str2","limit":100,"number_of_hits":42}]`

func TestStatistics(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_models.NewMockIModels(ctrl)

	m.EXPECT().Select(gomock.Any()).DoAndReturn(func(fbs *models.FizzBuzzes) error {
		*fbs = append(*fbs, models.FizzBuzz{
			Int1:   1,
			Int2:   2,
			Str1:   "str1",
			Str2:   "str2",
			Limit:  100,
			NbHits: 42,
		})
		return nil
	}).Times(1)

	status, res := handlers.Statistics(nil, m)

	if status != http.StatusOK {
		t.Error("bad status returned by Statistics handler")
	}

	if len(*res.(*models.FizzBuzzes)) != 1 {
		t.Error("bad number of results returned by Statistics handler")
	}

	jsonValue, err := json.Marshal(res)
	if err != nil {
		t.Fatal(err)
	}

	if string(jsonValue) != expectedStatisticsResult {
		t.Error("bad results returned by Statistics handler")
	}
}

func TestStatisticsFailedToSelect(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_models.NewMockIModels(ctrl)

	m.EXPECT().Select(gomock.Any()).Return(errors.New("failed to select")).Times(1)

	status, res := handlers.Statistics(nil, m)

	if status != http.StatusInternalServerError {
		t.Error("bad status returned by Statistics handler")
	}

	if res.(map[string]string)["error"] != "failed to select" {
		t.Error("bad error results returned by Statistics handler")
	}
}
