package handlers

import (
	"fizzbuzz/models"
	"fmt"
	"log"
	"strconv"
)

// IContext is an interface describing the context received by a handler
type IContext interface {
	QueryParam(name string) string
}

// Handler is a prototype for handler function
type Handler func(ctx IContext, m models.IModels) (int, interface{})

func formatError(err error) map[string]string {
	log.Print(err)
	return map[string]string{
		"error": err.Error(),
	}
}

func parseInt(ctx IContext, name string) (int, error) {
	arg := ctx.QueryParam(name)

	value, err := strconv.Atoi(arg)
	if err != nil {
		return 0, fmt.Errorf("bad value for argument %s", name)
	}

	return value, nil
}
