package handlers_test

import (
	"encoding/json"
	"errors"
	"fizzbuzz/mocks/mock_context"
	"fizzbuzz/mocks/mock_models"
	"fizzbuzz/models"
	"fizzbuzz/server/handlers"
	"net/http"
	"testing"

	"github.com/golang/mock/gomock"
)

const expectedFizzBuzzResult = `["1","foo","bar","foo","5","foobar"]`

func TestFizzBuzz(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_models.NewMockIModels(ctrl)
	c := mock_context.NewMockIContext(ctrl)

	c.EXPECT().QueryParam("str1").Return("foo").Times(1)
	c.EXPECT().QueryParam("str2").Return("bar").Times(1)
	c.EXPECT().QueryParam("int1").Return("2").Times(1)
	c.EXPECT().QueryParam("int2").Return("3").Times(1)
	c.EXPECT().QueryParam("limit").Return("6").Times(1)

	m.EXPECT().Save(gomock.Any()).DoAndReturn(func(fb models.FizzBuzz) error {
		if fb.Str1 != "foo" || fb.Str2 != "bar" ||
			fb.Int1 != 2 || fb.Int2 != 3 || fb.Limit != 6 {
			t.Error("bad values in the FizzBuzz")
		}
		return nil
	}).Times(1)

	status, res := handlers.FizzBuzz(c, m)

	if status != http.StatusOK {
		t.Error("bad status returned by FizzBuzz handler")
	}

	if len(res.([]string)) != 6 {
		t.Error("bad number of results returned by FizzBuzz handler")
	}

	jsonValue, err := json.Marshal(res)
	if err != nil {
		t.Fatal(err)
	}

	if string(jsonValue) != expectedFizzBuzzResult {
		t.Error("bad results returned by FizzBuzz handler")
	}
}

func TestFizzBuzzFailedToSave(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_models.NewMockIModels(ctrl)
	c := mock_context.NewMockIContext(ctrl)

	c.EXPECT().QueryParam("str1").Return("foo").Times(1)
	c.EXPECT().QueryParam("str2").Return("bar").Times(1)
	c.EXPECT().QueryParam("int1").Return("2").Times(1)
	c.EXPECT().QueryParam("int2").Return("3").Times(1)
	c.EXPECT().QueryParam("limit").Return("6").Times(1)

	m.EXPECT().Save(gomock.Any()).Return(errors.New("failed to save")).Times(1)

	status, res := handlers.FizzBuzz(c, m)

	if status != http.StatusInternalServerError {
		t.Error("bad status returned by FizzBuzz handler")
	}

	if res.(map[string]string)["error"] != "failed to save" {
		t.Error("bad error results returned by FizzBuzz handler")
	}
}

func TestFizzBuzzEmptyString(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := mock_context.NewMockIContext(ctrl)

	c.EXPECT().QueryParam("str1").Return("").Times(1)
	c.EXPECT().QueryParam("str2").Return("bar").Times(1)
	c.EXPECT().QueryParam("int1").Return("2").Times(1)
	c.EXPECT().QueryParam("int2").Return("3").Times(1)
	c.EXPECT().QueryParam("limit").Return("6").Times(1)

	status, res := handlers.FizzBuzz(c, nil)

	if status != http.StatusBadRequest {
		t.Error("bad status returned by FizzBuzz handler")
	}

	if res.(map[string]string)["error"] != "empty string is an invalid value" {
		t.Error("bad error results returned by FizzBuzz handler")
	}
}

func TestFizzBuzzZeroIntValue(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := mock_context.NewMockIContext(ctrl)

	c.EXPECT().QueryParam("str1").Return("foo").Times(1)
	c.EXPECT().QueryParam("str2").Return("bar").Times(1)
	c.EXPECT().QueryParam("int1").Return("0").Times(1)
	c.EXPECT().QueryParam("int2").Return("3").Times(1)
	c.EXPECT().QueryParam("limit").Return("6").Times(1)

	status, res := handlers.FizzBuzz(c, nil)

	if status != http.StatusBadRequest {
		t.Error("bad status returned by FizzBuzz handler")
	}

	if res.(map[string]string)["error"] != "integers should be strictly positive" {
		t.Error("bad error results returned by FizzBuzz handler")
	}
}

func TestFizzBuzzInvalidIntValue(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := mock_context.NewMockIContext(ctrl)

	c.EXPECT().QueryParam("str1").Return("foo").Times(1)
	c.EXPECT().QueryParam("str2").Return("bar").Times(1)
	c.EXPECT().QueryParam("int1").Return("baz").Times(1)

	status, res := handlers.FizzBuzz(c, nil)

	if status != http.StatusBadRequest {
		t.Error("bad status returned by FizzBuzz handler")
	}

	if res.(map[string]string)["error"] != "bad value for argument int1" {
		t.Error("bad error results returned by FizzBuzz handler")
	}

	c.EXPECT().QueryParam("str1").Return("foo").Times(1)
	c.EXPECT().QueryParam("str2").Return("bar").Times(1)
	c.EXPECT().QueryParam("int1").Return("2").Times(1)
	c.EXPECT().QueryParam("int2").Return("baz").Times(1)

	status, res = handlers.FizzBuzz(c, nil)

	if status != http.StatusBadRequest {
		t.Error("bad status returned by FizzBuzz handler")
	}

	if res.(map[string]string)["error"] != "bad value for argument int2" {
		t.Error("bad error results returned by FizzBuzz handler")
	}

	c.EXPECT().QueryParam("str1").Return("foo").Times(1)
	c.EXPECT().QueryParam("str2").Return("bar").Times(1)
	c.EXPECT().QueryParam("int1").Return("2").Times(1)
	c.EXPECT().QueryParam("int2").Return("3").Times(1)
	c.EXPECT().QueryParam("limit").Return("baz").Times(1)

	status, res = handlers.FizzBuzz(c, nil)

	if status != http.StatusBadRequest {
		t.Error("bad status returned by FizzBuzz handler")
	}

	if res.(map[string]string)["error"] != "bad value for argument limit" {
		t.Error("bad error results returned by FizzBuzz handler")
	}
}

func TestFizzBuzzSameValues(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := mock_context.NewMockIContext(ctrl)

	c.EXPECT().QueryParam("str1").Return("foo").Times(1)
	c.EXPECT().QueryParam("str2").Return("foo").Times(1)
	c.EXPECT().QueryParam("int1").Return("2").Times(1)
	c.EXPECT().QueryParam("int2").Return("3").Times(1)
	c.EXPECT().QueryParam("limit").Return("6").Times(1)

	status, res := handlers.FizzBuzz(c, nil)

	if status != http.StatusBadRequest {
		t.Error("bad status returned by FizzBuzz handler")
	}

	if res.(map[string]string)["error"] != "str1 can't be equal to str2" {
		t.Error("bad error results returned by FizzBuzz handler")
	}

	c.EXPECT().QueryParam("str1").Return("foo").Times(1)
	c.EXPECT().QueryParam("str2").Return("bar").Times(1)
	c.EXPECT().QueryParam("int1").Return("2").Times(1)
	c.EXPECT().QueryParam("int2").Return("2").Times(1)
	c.EXPECT().QueryParam("limit").Return("6").Times(1)

	status, res = handlers.FizzBuzz(c, nil)

	if status != http.StatusBadRequest {
		t.Error("bad status returned by FizzBuzz handler")
	}

	if res.(map[string]string)["error"] != "int1 can't be equal to int2" {
		t.Error("bad error results returned by FizzBuzz handler")
	}
}
