package handlers

import (
	"fizzbuzz/models"
	"net/http"
)

// Statistics is a handler that returns the FizzBuzz statistics
func Statistics(ctx IContext, m models.IModels) (int, interface{}) {
	fbs := &models.FizzBuzzes{}
	if err := m.Select(fbs); err != nil {
		return http.StatusInternalServerError, formatError(err)
	}

	return http.StatusOK, fbs
}
