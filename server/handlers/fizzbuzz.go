package handlers

import (
	"errors"
	"fizzbuzz/models"
	"net/http"
	"strconv"
)

func parseFizzBuzzArgs(ctx IContext) (models.FizzBuzz, error) {
	str1 := ctx.QueryParam("str1")

	str2 := ctx.QueryParam("str2")

	int1, err := parseInt(ctx, "int1")
	if err != nil {
		return models.FizzBuzz{}, err
	}

	int2, err := parseInt(ctx, "int2")
	if err != nil {
		return models.FizzBuzz{}, err
	}

	limit, err := parseInt(ctx, "limit")
	if err != nil {
		return models.FizzBuzz{}, err
	}

	return models.FizzBuzz{Str1: str1, Str2: str2, Int1: int1, Int2: int2, Limit: limit}, nil
}

func validateFizzBuzzArgs(fb models.FizzBuzz) error {
	if fb.Str1 == "" || fb.Str2 == "" {
		return errors.New("empty string is an invalid value")
	}

	if fb.Str1 == fb.Str2 {
		return errors.New("str1 can't be equal to str2")
	}

	if fb.Int1 <= 0 || fb.Int2 <= 0 || fb.Limit <= 0 {
		return errors.New("integers should be strictly positive")
	}

	if fb.Int1 == fb.Int2 {
		return errors.New("int1 can't be equal to int2")
	}

	return nil
}

func executeFizzBuzz(fb models.FizzBuzz) []string {
	res := []string{}

	for i := 1; i <= fb.Limit; i++ {
		elem := ""

		if i%fb.Int1 == 0 {
			elem += fb.Str1
		}

		if i%fb.Int2 == 0 {
			elem += fb.Str2
		}

		if elem == "" {
			elem = strconv.Itoa(i)
		}

		res = append(res, elem)
	}

	return res
}

// FizzBuzz is the handler that runs a FizzBuzz
func FizzBuzz(ctx IContext, m models.IModels) (int, interface{}) {
	fb, err := parseFizzBuzzArgs(ctx)
	if err != nil {
		return http.StatusBadRequest, formatError(err)
	}

	if err := validateFizzBuzzArgs(fb); err != nil {
		return http.StatusBadRequest, formatError(err)
	}

	if err := m.Save(fb); err != nil {
		return http.StatusInternalServerError, formatError(err)
	}

	res := executeFizzBuzz(fb)

	return http.StatusOK, res
}
