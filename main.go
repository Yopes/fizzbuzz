package main

import (
	"fizzbuzz/database"
	"fizzbuzz/models"
	"fizzbuzz/server"
	"fizzbuzz/server/handlers"
	"log"
	"net/http"
)

const (
	databaseFileName = "./fizzbuzz.db"
	httpPort         = 8080
)

func main() {
	db, err := database.New(databaseFileName)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	m, err := models.New(db)
	if err != nil {
		log.Fatal(err)
	}

	s := server.New(m)
	s.Subscribe(http.MethodGet, "/fizzbuzz", handlers.FizzBuzz)
	s.Subscribe(http.MethodGet, "/statistics", handlers.Statistics)

	if err := s.Run(httpPort); err != nil {
		log.Fatal(err)
	}
}
