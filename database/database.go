package database

import (
	"database/sql"

	// Needed to use sqlite3
	_ "github.com/mattn/go-sqlite3"
)

// IDatabase is an interface describing a Database object
type IDatabase interface {
	Exec(query string, args ...interface{}) error
	Prepare(query string) (IStatement, error)
	Query(query string, args ...interface{}) (IRows, error)
	Close()
}

// Database is an abstraction of a database
type Database struct {
	db *sql.DB
}

// New creates a new Database
func New(name string) (IDatabase, error) {
	db, err := sql.Open("sqlite3", name)
	if err != nil {
		return nil, err
	}

	return &Database{db: db}, nil
}

// Close closes the Database
func (database *Database) Close() {
	database.db.Close()
}

// Exec executes a query in the Database
func (database *Database) Exec(query string, args ...interface{}) error {
	_, err := database.db.Exec(query, args...)
	return err
}

// IStatement is an interface describing a Statement
type IStatement interface {
	Exec(args ...interface{}) error
}

// Statement is a prepared statement for a later query
type Statement struct {
	stmt *sql.Stmt
}

// Prepare prepares a statement for a later query
func (database *Database) Prepare(query string) (IStatement, error) {
	stmt, err := database.db.Prepare(query)
	if err != nil {
		return nil, err
	}

	return &Statement{stmt: stmt}, nil
}

// Exec executes a prepared Statement
func (statement *Statement) Exec(args ...interface{}) error {
	_, err := statement.stmt.Exec(args...)
	return err
}

// IRows is an interface describing a Rows
type IRows interface {
	Next() bool
	Scan(dest ...interface{}) error
	Close() error
}

// Rows is the result of a Query
type Rows struct {
	rows *sql.Rows
}

// Query executes a query and returns the result as an IRows
func (database *Database) Query(query string, args ...interface{}) (IRows, error) {
	rows, err := database.db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	return &Rows{rows: rows}, nil
}

// Next prepares the next result, it returns false if there is no more results
func (rows *Rows) Next() bool {
	return rows.rows.Next()
}

// Scan copies the column from the result row into the values pointed by dest
func (rows *Rows) Scan(dest ...interface{}) error {
	return rows.rows.Scan(dest...)
}

// Close closes the result rows
func (rows *Rows) Close() error {
	return rows.rows.Close()
}
