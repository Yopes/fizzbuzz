package models_test

import (
	"errors"
	"fizzbuzz/mocks/mock_database"
	"fizzbuzz/models"
	"testing"

	"github.com/golang/mock/gomock"
)

const (
	createTableRequest = `
		create table if not exists fizzbuzz (
			id integer not null primary key,
			int1 integer,
			int2 integer,
			str1 text,
			str2 text,
			lmt integer);
	`

	selectRequest = `SELECT int1, int2, str1, str2, lmt, COUNT(*) as nb_hits
		FROM fizzbuzz
		GROUP BY int1, int2, str1, str2, lmt
		ORDER BY nb_hits DESC
	`
)

func TestCantCreateTable(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := mock_database.NewMockIDatabase(ctrl)

	db.EXPECT().Exec(createTableRequest).
		Return(errors.New("can't create table")).Times(1)

	_, err := models.New(db)

	if err == nil || err.Error() != "can't create table" {
		t.Error("models.New didn't returned the expected error")
	}
}

func TestSave(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := mock_database.NewMockIDatabase(ctrl)

	db.EXPECT().Exec(createTableRequest).Return(nil).Times(1)

	m, err := models.New(db)

	if err != nil {
		t.Error("models.New returned an error")
	}

	fb := models.FizzBuzz{
		Str1:  "str1",
		Str2:  "str2",
		Int1:  1,
		Int2:  2,
		Limit: 100,
	}

	stmt := mock_database.NewMockIStatement(ctrl)

	stmt.EXPECT().Exec(fb.Int1, fb.Int2, fb.Str1, fb.Str2, fb.Limit).Return(nil).Times(1)

	db.EXPECT().Prepare("INSERT INTO fizzbuzz (int1, int2, str1, str2, lmt) VALUES (?, ?, ?, ?, ?)").
		Return(stmt, nil).Times(1)

	if err = m.Save(fb); err != nil {
		t.Error("models.Save returned an error")
	}
}

func TestSaveFailedToPrepare(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := mock_database.NewMockIDatabase(ctrl)

	db.EXPECT().Exec(createTableRequest).Return(nil).Times(1)

	m, err := models.New(db)

	if err != nil {
		t.Error("models.New returned an error")
	}

	fb := models.FizzBuzz{
		Str1:  "str1",
		Str2:  "str2",
		Int1:  1,
		Int2:  2,
		Limit: 100,
	}

	db.EXPECT().Prepare("INSERT INTO fizzbuzz (int1, int2, str1, str2, lmt) VALUES (?, ?, ?, ?, ?)").
		Return(nil, errors.New("failed to prepare")).Times(1)

	if err = m.Save(fb); err == nil || err.Error() != "failed to prepare" {
		t.Error("models.Save didn't returned the expected error")
	}
}

func TestSaveFailedToExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := mock_database.NewMockIDatabase(ctrl)

	db.EXPECT().Exec(createTableRequest).Return(nil).Times(1)

	m, err := models.New(db)

	if err != nil {
		t.Error("models.New returned an error")
	}

	fb := models.FizzBuzz{
		Str1:  "str1",
		Str2:  "str2",
		Int1:  1,
		Int2:  2,
		Limit: 100,
	}

	stmt := mock_database.NewMockIStatement(ctrl)

	stmt.EXPECT().Exec(fb.Int1, fb.Int2, fb.Str1, fb.Str2, fb.Limit).
		Return(errors.New("failed to exec")).Times(1)

	db.EXPECT().Prepare("INSERT INTO fizzbuzz (int1, int2, str1, str2, lmt) VALUES (?, ?, ?, ?, ?)").
		Return(stmt, nil).Times(1)

	if err = m.Save(fb); err == nil || err.Error() != "failed to exec" {
		t.Error("models.Save didn't returned the expected error")
	}
}

func TestSelect(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := mock_database.NewMockIDatabase(ctrl)

	db.EXPECT().Exec(createTableRequest).Return(nil).Times(1)

	m, err := models.New(db)

	if err != nil {
		t.Error("models.New returned an error")
	}

	fbs := models.FizzBuzzes{}

	rows := mock_database.NewMockIRows(ctrl)

	rows.EXPECT().Next().Return(false).After(rows.EXPECT().Next().Return(true))
	rows.EXPECT().Scan(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		DoAndReturn(func(int1 *int, int2 *int, str1 *string, str2 *string, limit *int, nbHits *int) error {
			*int1 = 1
			*int2 = 2
			*str1 = "str1"
			*str2 = "str2"
			*limit = 100
			*nbHits = 42
			return nil
		}).Times(1)
	rows.EXPECT().Close().Times(1)

	db.EXPECT().Query(selectRequest).Return(rows, nil).Times(1)

	if err = m.Select(&fbs); err != nil {
		t.Error("models.Select returned an error")
	}

	if len(fbs) != 1 || fbs[0].Int1 != 1 || fbs[0].Int2 != 2 ||
		fbs[0].Str1 != "str1" || fbs[0].Str2 != "str2" ||
		fbs[0].Limit != 100 || fbs[0].NbHits != 42 {
		t.Error("bad values for the selected FizzBuzz")
	}
}

func TestSelectFailedToQuery(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := mock_database.NewMockIDatabase(ctrl)

	db.EXPECT().Exec(createTableRequest).Return(nil).Times(1)

	m, err := models.New(db)

	if err != nil {
		t.Error("models.New returned an error")
	}

	fbs := models.FizzBuzzes{}

	db.EXPECT().Query(selectRequest).Return(nil, errors.New("failed to query")).Times(1)

	if err = m.Select(&fbs); err == nil ||
		err.Error() != "failed to query" {
		t.Error("models.Select didn't returned the expected error")
	}
}

func TestSelectFailedToScan(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := mock_database.NewMockIDatabase(ctrl)

	db.EXPECT().Exec(createTableRequest).Return(nil).Times(1)

	m, err := models.New(db)

	if err != nil {
		t.Error("models.New returned an error")
	}

	fbs := models.FizzBuzzes{}

	rows := mock_database.NewMockIRows(ctrl)

	rows.EXPECT().Next().Return(true)
	rows.EXPECT().Scan(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
		Return(errors.New("failed to scan")).Times(1)
	rows.EXPECT().Close().Times(1)

	db.EXPECT().Query(selectRequest).Return(rows, nil).Times(1)

	if err = m.Select(&fbs); err == nil ||
		err.Error() != "failed to scan" {
		t.Error("models.Select didn't returned the expected error")
	}
}
