package models

import (
	"fizzbuzz/database"
)

// FizzBuzz is an object containing all the arguments of a FizzBuzz run
// it also contains the number of hits of the configuration
type FizzBuzz struct {
	Int1   int    `json:"int1"`
	Int2   int    `json:"int2"`
	Str1   string `json:"str1"`
	Str2   string `json:"str2"`
	Limit  int    `json:"limit"`
	NbHits int    `json:"number_of_hits"`
}

// FizzBuzzes is a Slice of FizzBuzz
type FizzBuzzes []FizzBuzz

// Save stores the FizzBuzz in the given IDatabase
func (fb FizzBuzz) Save(db database.IDatabase) error {
	statement, err := db.Prepare("INSERT INTO fizzbuzz (int1, int2, str1, str2, lmt) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		return err
	}
	return statement.Exec(fb.Int1, fb.Int2, fb.Str1, fb.Str2, fb.Limit)
}

// Select retreives all FizzBuzzes from the given IDatabase
// it groups them by configuration and counts the number of hits
func (fbs *FizzBuzzes) Select(db database.IDatabase) error {
	rows, err := db.Query(`SELECT int1, int2, str1, str2, lmt, COUNT(*) as nb_hits
		FROM fizzbuzz
		GROUP BY int1, int2, str1, str2, lmt
		ORDER BY nb_hits DESC
	`)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		fb := FizzBuzz{}
		err := rows.Scan(&fb.Int1, &fb.Int2, &fb.Str1, &fb.Str2, &fb.Limit, &fb.NbHits)
		if err != nil {
			return err
		}

		*fbs = append(*fbs, fb)
	}

	return nil
}
