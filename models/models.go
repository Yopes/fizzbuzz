package models

import "fizzbuzz/database"

// IModels is an interface describing a Models object
type IModels interface {
	Save(s Savable) error
	Select(s Selectable) error
}

// Models is an object that manages datbase related objects
type Models struct {
	db database.IDatabase
}

// New returns a new Models and creates tables in the datbase
func New(db database.IDatabase) (IModels, error) {
	if err := db.Exec(`
		create table if not exists fizzbuzz (
			id integer not null primary key,
			int1 integer,
			int2 integer,
			str1 text,
			str2 text,
			lmt integer);
	`); err != nil {
		return nil, err
	}

	return &Models{db: db}, nil
}

// Savable is an object that can save itself to an IDatabase
type Savable interface {
	Save(db database.IDatabase) error
}

// Selectable is an object that can select itself from an IDatabase
type Selectable interface {
	Select(db database.IDatabase) error
}

// Save executes the Save method of a Savable object
func (m *Models) Save(s Savable) error {
	return s.Save(m.db)
}

// Select executes the Select method of a Selectable object
func (m *Models) Select(s Selectable) error {
	return s.Select(m.db)
}
